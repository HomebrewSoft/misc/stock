from odoo import _, api, models
from odoo.exceptions import ValidationError


class StockMove(models.Model):
    _inherit = "stock.move"

    def _action_done(self, cancel_backorder=False):
        for record in self:
            if record.company_id.allow_negative_qty or record.location_id.usage != "internal":
                continue

            quants = self.env["stock.quant"].search(
                [
                    ("company_id", "=", record.company_id.id),
                    ("location_id", "child_of", record.location_id.id),
                    ("product_id", "=", record.product_id.id),
                ]
            )
            total_qty = sum(quants.mapped("quantity"))
            if record.quantity_done > total_qty:
                raise ValidationError(
                    _(
                        f"Quantity of {record.product_id} cannot be supplied from {record.location_id}"
                    )
                )
        return super(StockMove, self)._action_done(cancel_backorder)
