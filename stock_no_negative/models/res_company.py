from odoo import fields, models


class Company(models.Model):
    _inherit = "res.company"

    allow_negative_qty = fields.Boolean()
