from odoo import fields, models


class ConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    allow_negative_qty = fields.Boolean(
        related="company_id.allow_negative_qty",
        readonly=False,
    )
